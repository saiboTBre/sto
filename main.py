import statistics as stat
import numpy as np
import scipy.integrate as integrate
import scipy.signal as signal


l = [1,1,1,2,3,3,4,5,5]
l1 = [1,1,3,2,3,3,4,1,5]

j= [25,17,25,29,20,15,11,17,16,16]
p= [11,11,13,9,10,7,3,8,9,4]
r= [0,4,0,0,0,2,2,1,3,0]

rjp= np.corrcoef(j,p)
rjr= np.corrcoef(j,r)
rpr= np.corrcoef(p,r)
print(rjp)
print(rjr)
print(rpr)

mean = stat.mean(l)
median = stat.median(l)
stdev = stat.stdev(l)
variance = stat.variance(l)
modal = stat.multimode(l)

# covariance
array1 = np.array([3,1,4])
array2 = np.array([5,1,3])

deciles = [round(q, 1) for q in stat.quantiles(l, n=10)]
quartiles = [round(q, 1) for q in stat.quantiles(l, n=4)]

iqr = quartiles[2] - quartiles[0]
range = l[len(l)-1]-l[0]

covariance = np.cov(array1, array2)[0][1]
correlation = np.correlate(l, l1)
corrcoef = np.corrcoef(l, l1)

print("Durschnitt:" + str(mean))
print("Median:" + str(median))
print("Standardabweichung:" + str(stdev))
print("Deciles:" + str(deciles))
print("Quartile:" + str(quartiles))
print("Modal:" + str(modal))
print("IQR:" + str(iqr))
print("Spannweite:" + str(range))
print("Kovarianz:" + str(covariance))
print("Korrelation:" + str(correlation))
print("Korrelationkoeffizeint:" + str(corrcoef[0,1]))


# Kombinatorik

# Bermoulli
def ber_erwartungswert(p):
    return p

def ber_variance(p):
    return p*(1-p)

# Geometrische Verteilung
def geo_erwartungswert(p):
    return 1/p

def geo_variance(q, p):
    return q/p**2


# Binomial Verteilung
def bin_erwartungswert(n,p):
    return n*p

def bin_variance(n, p):
    return n*p*(1-p)

# Hyper Verteilung
def hyper_erwartungswert(n,M,N):
    return n*(M/N)

def hyper_variance(n, M, N):
    return n*(M/N)*(1-M/N)*((N-n)/(N-1))

# Poisob Verteilung
def po_erwartungswert(lam):
    return lam

def po_variance(lam):
    return lam

