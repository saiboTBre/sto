# -*- coding: utf-8 -*-
import numpy as np
from scipy.stats import uniform, expon, norm
import scipy


# X kann NICHT einen konkreten Wert annehmen => P(X=x) bei stetig = 0

# Whs != Dichte => P(X=x) != f(x)

# not necessary helper function for displaying results a bit nicer
def myprint(x):
    x = str(np.round(x,3))
    return x

# Gleichverteilung
# Eve liest Meldungen in ihrer Twitter-Timeline.
# Die Zeit, die sie für das Lesen eines Beitrags braucht 
# wird durch die Zufallsvariable X ∼ U(5, 300) für die Lesezeit in Sekunden

# X~U(a,b)

binom = scipy.special.binom(49,6)
binom1 = scipy.special.binom(6,6)
binom2 = scipy.special.binom(10,1)
print("Binom: " + myprint(binom))
print("Binom1: " + myprint(binom1))
# print("Binom2: " + myprint(binom2))

def gleichverteilt(a, b, lt=0, gt=0):
    print("------------------------")
    print("GLEICH")
    print("------------------------")

    s = b-a
    X = uniform(loc=a, scale=s)

    P_XkleinergleichA = X.cdf(lt)
    print('P_Xkleinergleich1 = ' + myprint(P_XkleinergleichA))

# P_Xmehrals1 = X.sf(60)
    P_XmehralsB = X.sf(gt)
    print('P_Xmehrals1 = ' + myprint(P_XmehralsB))


    E_X = X.mean()
    print('E_X = ' + myprint(E_X))
    
    std_X = X.std()
    print('std_X = ' + myprint(std_X))

print(gleichverteilt(5, 300, 60, 60))

# Exponentialverteilung
# Ein Paar Laufschuhe hält im Mittel 18 Monate, falls diese täglich
# benutzt werden. Die Zeit, die Laufschuhe nutzbar sind, bevor sie
# kaputt gehen, ist also Y~expon(1/18)

def exponential(lamb, gt=0, zw1=0,zw2=0, quant=0): 

    print("------------------------")
    print("EXPONENTIONAL")
    print("------------------------")

    Y = expon(loc=0, scale=1/lamb)

    P_YgroessergleichA = Y.sf(gt)
    print('P_Y >= ' + myprint(gt) + ' = ' + myprint(P_YgroessergleichA))

    P_YzwischenAundB = Y.cdf(zw2) - Y.cdf(zw1)
    print('P_YzwischenAundB = ' + myprint(P_YzwischenAundB))

    Quant = Y.ppf(quant)
    print('Quant_Y = ' + myprint(Quant))

    E_Y = Y.mean()
    print('E_Y = ' + myprint(E_Y))

    std_Y = Y.std()
    print('std_Y = ' + myprint(std_Y))

    print(' ')



# print(exponential(1/18, 15, 12,15,0.75))
print(exponential(1/7, (2/7), 6, 8,0.9))

# Normalverteilung
# Die Größe eines zufällig ausgewählten deutschen Mannes ist eine
# Zufallsvariable X ∼ N(180.3, 7.17).

def normalverteilt(mu=0, sig=0):
    
    Z = norm(loc=mu,scale=sig)

    print('------------------------')
    print('NORMAL')
    print('zB Körpergröße')
    print('------------------------')


    # Kleiner gleich
    # P_ZkleinergleichX = Z.cdf(175)
    P_ZkleinergleichX = Z.cdf(12)
    print('P_Zkleinergleich X = ' + myprint(P_ZkleinergleichX))

    P_ZgroessergleichX = Z.sf(200)
    print('P_Zgroessergleich X = ' + myprint(P_ZgroessergleichX))

    P_ZzwischenX_Y = Z.sf(11)
    P_ZzwischenY_X = Z.sf(15)
    print('P_Zzwischen X_Y X = ' + myprint(P_ZzwischenX_Y - P_ZzwischenY_X))

    Quant_X = Z.ppf(0.93)
    print('Quant_X = ' + myprint(Quant_X))

    E_Z = Z.mean()
    print('E_Z = ' + myprint(E_Z))

    std_Z = Z.std()
    print('std_Z = ' + myprint(std_Z))

    print(' ')

    print('Zwischen 2 Quantilen => in welches intervall fallen 90% = 0.05 und 0.95')
    print('[0.05-Quantil, 0.95-Quantil] = [' + myprint(Z.ppf(0.05)) + ', ' 
          + myprint(Z.ppf(0.95)) + ']')

    print(' ')

# normalverteilt(180, 7.17)
normalverteilt(13, 1)
